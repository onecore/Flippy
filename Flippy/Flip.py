__author__ = "Mark Anthony R. Pequeras"
__version__ = "2.0"
__license__ = ["MIT"]
__website__ = "http://marp.me/flippy"
__git__ = "https://gitlab.com/coresec/Flip.git"
__name__ = "Flippy Library"
__build__ = ["OS X"]
__info__ = """

Flip.py (Flippy) is a Python Library for changing Python's Reserved word or any library, methods, symbols into something
you set it to be, No limit as long as you do it right.

"""

# That's All we need.
import os, inspect, importlib

__all__ = "Py"

# Variable with current directory path
__cur__ = os.getcwd()

# TypeCheck(er) function for checking Types, TypeCheck(object,type) > True,False
TypeCheck = lambda object,type: True if isinstance(object,type) else False

# Custom Exception
class FlippyError(Exception):
    pass


class Py:

    global UserSyntax, Parsed, SyntaxPreset
    UserSyntax, Parsed, SyntaxPreset = None, None, False

    def __init__(self,Source,Syntax=None,Debug=False):
        """
        Initialize Defined Parameters.
        """

        self.Check(Source,Syntax)
        self._Source = Source
        self._Translated = None
        self._Debugger = Debug
        self._Syntax = None

        if not TypeCheck(Syntax,dict) and TypeCheck(Syntax,str): # Preset On
            self._Syntax = self.__Syntax(Syntax)
            self.FlippyDebug("Syntax Preset: {}".format(Syntax))

        elif TypeCheck(Syntax,dict) and not TypeCheck(Syntax,str): # Preset Off
            self._Syntax = Syntax
            self.FlippyDebug("Syntax Preset None.")



    # Check for some needed objects, Runs on Start
    def Check(self,Source,Syntax):
        """
        Parameters Check on Start
        """
        global SyntaxPreset

        if not Source:
            raise FlippyError("Source Argument (Strings) Required")
            return False

        if not Syntax:
            if not Syntax and SyntaxPreset:
                pass
            else:
                raise FlippyError("Syntax Argument (Dictionary) Required")
                return False

        if not TypeCheck(Source,str):
            raise FlippyError("Source must be String, Could'nt Iterate the Source.")
            return False



    # Shows some information about the process
    def FlippyDebug(self,out,custom=False):
        """
        A Function decorator for showing I/O of a Flippy function
        """
        _function = inspect.stack()[1][3]

        if not custom and self._Debugger:
            print "FlippyDebug(): "+str(out)+" from function "+str(_function)+"()"
        elif custom and self._Debugger:
            print "FlippyDebug(): "+str(out)

    # Just to make the lib beautiful rather than call the Parser() method directly
    def Compile(self):
        """
        A Function that does, Copy your whole Source, Write into a .py files and Run.
        """
        self.FlippyDebug("Calling Parser() ")
        self.Parser()



    # Translates your Custom syntax into its Original value
    def Parser(self):
        """
        Translate your Defined syntax into its original (Python)
        """
        global Parsed, UserSyntax, SyntaxPreset

        _kCache = []
        _vCache = []
        _Cache = None
        _mSource = self._Source
        _Lexer = None
        try:
            for kSyntax in self._Syntax.iterkeys():
                 _kCache.append(kSyntax)

            for vSyntax in self._Syntax.itervalues():
                _vCache.append(vSyntax)

            self.FlippyDebug(str(len(_kCache))+" syntax added for parse")

        except:
            raise FlippyError("Could'nt Iterate inside the Syntax object!")

        try:
            for Syntax in _mSource:
                for k in _kCache:
                    while k in _mSource:
                        _mSource = _mSource.replace(str(k),str(self._Syntax[k]),9999999) # Clean Parsed
                        if k not in _mSource:
                            break
            Parsed = _mSource

            self.FlippyDebug("Source with Syntax Parsed successfully..")

        except TypeError:
            raise FlippyError("Cannot Iterate the Source (Strings)")



    # If you want to use the Flippy's Preset Syntax template, this function will do the process
    def __Syntax(self,SyntaxFile=None):
        """
        Use any Preset Syntax by Passing it to SyntaxFile Parameter,
        Make sure to set this Function on Top of other Flippy methods.
        """
        global SyntaxPreset

        Import = "Flippy.Syntax."+SyntaxFile

        if SyntaxFile:

            try:
                Import = importlib.import_module(Import)

                if Import.Syntax:
                    self.FlippyDebug("Loaded Syntax template: "+SyntaxFile)

                if Import.Name:
                    self.FlippyDebug("Syntax Name: "+SyntaxFile)

                if Import.Keywords:
                    self.FlippyDebug("Successfully Loaded custom Keywords!")

                return Import.Keywords
            except:
                raise FlippyError(SyntaxFile+" Is not a Syntax Template!")

        else:
            self.FlippyDebug("Syntax is not Set")

    # I'm gonna be writing your Syntax and other Info inside the 'Syntax' Folder.
    def SyntaxWrite(self):
        """
        Add your prefered syntax, by passing it as keyword arguments and saving it,
        into a preloadable config file
        """

        pass

    # So you want to Improve your Performance? Call me and do not forget to have a Cython.
    def Cythonize(self):
        """
        Add-on function for Compiling your Source using Cython Compiler,
        Make sure to have one, else raise will be called.
        """
        Cap = None
        try:
            import cython
            Cap = 0
            return False

        except ImportError:
            Cap = None
            self.FlippyDebug("'cython' is not installed ")

        try:
            import Cython
            Cap = 1
            return False

        except ImportError:
            Cap = None
            self.FlippyDebug("'Cython' is not installed ")




    # I'm gonna be generating your Source on every run.
    def GenerateSource(self,name="Generated.py"):
        """
        Generate an original copy of your code into Python (.py) file
        """
        import os
        global Parsed
        Dir = "Source"

        if '.py' not in name:
            name = name + ".py"

        try:
            os.mkdir(Dir)

        except OSError:
            self.FlippyDebug("ERROR Directory"+Dir+" Exist.. ")
            pass

        try:
            stringSouce = Parsed
            RawSource = """
# -*- coding: utf-8 -*-

# Files Generated by {fpy}
{source}
            """.format(fpy=__name__,source=stringSouce)

            with open(name, "w") as _sourcefile:
                _sourcefile.write(RawSource)

        except:
            pass

    # This will run the GenerateSource() method and call Run (Your generated/translated .py will exec)
    def RunGenerate(self,name):
        """
        Generate an original copy of your code into Python (.py) file and Run afterwards.
        """
        if not name:
            self.FlippyDebug("name=string parameter required!")
            raise FlippyError("Parameter Required, String")
            return False

        else: pass
        self.GenerateSource(name=name)
        filePy = lambda: ".py" if ".py" not in name else None
        self.Run(name+filePy())

    # I'm gonna be runnin your Source with Syntax, feel free to change me.
    def Run(self,file=None,throughpy=False):
        """
        Run your Source without Generating Any Python (.py) files.
        """
        global Parsed

        self.FlippyDebug("Starting Process")

        if not file:
            try:
                self.FlippyDebug("Finished, with the Output: \n",True)
                try:
                    Comp = compile(Parsed,"<string>","exec")
                    exec Comp
                except:
                    raise FlippyError("Source String could'nt Parse and compile.")

            except TypeError:
                raise FlippyError("Unable to Process the Source, call Compile() or Parse() first before Run!")

        elif file:
            execfile(file)