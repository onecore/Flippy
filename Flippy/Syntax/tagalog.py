Author = "Mark Pequeras"

Syntax = True

Name = "Tagalog Syntax (Not literal.)"

Information = """
Keywords below are translated into Tagalog language (Not Literal)
"""

Keywords = {


    "at" : "and"
    , "burahin" : "del"
    , "iangkat" : "import"
    , "ipangalan ay" : "as"
    , "ayaw eh" : "raise"
    , "isaysay" : "assert"
    , "i hinto" : "break"
    , "igrupo sa" : "class"
    , "tuloy lang" : "continue"
    , "gawin ang" : "def"
    , "subukan" : "try"
    , "kung ayaw" : "except"
    , "sa wakas" : "finally"
    , "ay totoo" : "is"
    , "ibalik" : "return"
    , "sa mga" : "for"
    , "sa" : "in"
    , "patakbuhin" : "exec"
    , "galing sa" : "from"
    , "ipakita ang" : "print"
    , "hindi" : "not"
    , "hanggat" : "while"
    , "habang" : "with"
    , "o kaya" : "or"
    , "pang lahatan" : "global"
    , "lambda" : "lambda"
    , "palagpasin" : "pass"
    , "hinto at" : "yield"
    , "kung" : "if"
    , "kung sakaling" : "elif"
    , "kung wala" : "else"
    , "ay" : "="
    , "parehas" : "=="
    , "mas malaki sa" : ">"
    , "mas maliit sa" : "<"
    , "hindi parehas" : "!="
    , "malaki at parehas" : ">="
    , "maliit at parehas" : "=<"
    , "totoo" : "True"
    , "wala" : "None"
    , "hindi totoo" : "False"

}


