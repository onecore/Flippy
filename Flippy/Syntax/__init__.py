"""
This Directory Contains some Preset Syntax

Information:
File:   nameofsyntax.py (The Name of Custom Syntax or "Syntax Template as Flippy calls it.")

Inside of the File:

'Syntax' (BOOL) Variable (Must be set to True, False if not a Syntax template

'Name' (String, Long String) Variable (A Custom Name of Syntax, Single or Multi-line is fine.

'Keywords' (Dictionary) Variable (Contains the Custom word 'syntax' as Key and a Python Reserved word as Value)

"""