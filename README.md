FlipPy (Flip Python / Filipino Python) is a Python Library for Parsing defined Python Programming Language syntax into your preferred syntax,

It's like creating a new Clone of Python with your Own preferred Syntax without touching any mighty Python's files.

![Version](https://img.shields.io/badge/Version-1.0.0-orange.svg?style=flat-square)

![Status](https://img.shields.io/badge/FlipPY-Beta-orange.svg?style=flat-square) 

![website](https://img.shields.io/badge/Source-marp.me/flippy-orange.svg?style=flat-square) 

![author](https://img.shields.io/badge/Author-Mark Anthony Pequeras-orange.svg?style=flat-square) 

![authorsite](https://img.shields.io/badge/Website-marp.me-orange.svg?style=flat-square) 

---------------
**Beta Usage**

```python

from Flip import Py


MyCustomSyntax = {

            {"kung":"if",
            "....":"    ",
            "ay":"=",
            "palagpasin":"pass",
            "gawin":"define",
            "sakaling":"elif",
            "kungwala":"else",
            "ipakita ang":"print",
            "ay parehas sa":"==",

}


MyCustomSource = """

mark ay 1
markie ay 1


kung mark:
....ipakita ang mark + markie

sakaling mark ay parehas sa markie:
....ipakita "Hi!"   

kungwala:   
....palagpasin

"""


Flippy = Py( MyCustomSource , MyCustomSyntax) 
Flippy.Compile()
Flippy.Run()

# Output:  '2'

```

-----------

**Equivalent to Python source**

```python

mark = 1
markie = 1

if mark:
    print mark + markie

elif mark == markie:
    print "Hi!"

else:
    pass
```