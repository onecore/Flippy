
Basic Usage Information

First, Here are the Information of the Methods we are
going to use.

---------------
**Py** Class
==============
`from flippy.Flip import Py`

Parameters:
> - **Source**   An argument to set our short/long Strings, Accept String type
> - **Syntax**   An argument to set our custom syntax/word, Accept Dictionary type
> - **Debug**    Shows output of every process, It's not noisy don't worry, Accept Bool (True or False)

Example: `Py( Source=MySourceVariable, Syntax=MySyntaxVariable, Debug = False )`

------------------

**Compile** method
==========================
`Py.Compile()`

> - Method to parse your Source & Syntax, 

Example: 

```
myprogram = Py(Source=X, Syntax=Y)
myprogram.Compile()  
```
------------------


**Run** method
==============
`Py.Run()`

> - Method to Run your Flippy parsed Source & Syntax

Example:

```
myprogram = Py(Source=X, Syntax=Y)
myprogram.Compile()
myprogram.Run()
```


------


**Putting it all together**
===========================

```
from flippy import Flip

MySource = """
show me 'Hello Python.'
"""

MySyntax = { "show me" : "print" } 

Flippy = Flip.Py(Source, Syntax)
Flippy.Compile() 
Flippy.Run()
```

Output: `Hello Python.`





