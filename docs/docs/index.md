# Welcome to Flippy!
<a href=""><img src="http://marp.me/flippy/flippy.png" align="left" height="200" width="200" ></a>


## Information

* **Flippy** (Flip.Py) a Python Module for Translating any given words  back to its Original using my parser, This helps you if you are willing to change any of word/string in your Source, such as Python Keywords, Methods and Imports.

* **Useful for** Educational purposes, Works on diffirent kinds of Python Frameworks (as of exec compile()), Distribute Python with an awesome poetry like syntax and more.

## Sample Flippy

``` python
from Flippy import Flip

MyCustomSyntax = {

	"i want" : "def" ,
	"show me" : "print"
	
}


MyCustomSource = """

i want This(food):
	show me food

This('Cake')

"""


Parse = Flip.Py(MyCustomSource, MyCustomSyntax)
Parse.Compile()
Parse.Run()
```
* Check the Menu for more Example, Information and Documentations!