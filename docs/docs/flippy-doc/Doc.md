Full Information about the Py class and its methods.


The Class
=========
`from flippy.Flip import Py`

**Py**( Source, Syntax )

`Py()` class contains `Source` and `Syntax` argument and both of them are needed before calling other methods, format `Py(Source, Syntax)`.

Arguments Information:

* **Source**   (string) Accept long,short strings
	* Must be a String with a custom word/syntax
* **Syntax**    (dictionary)  Accept only strings 
	* Must be a Dictionary with a pair of "word":"original", or a Custom syntax/word with the original which is accepted by Python.

------------------------

The Methods
===========

------

`Py.Compile()`


**Coampile**()

a function that calls Parse (which parse your Source with Syntax), Planning to make it run with Py() on Init soon, but for now just use it before any other method.

-----------

`Py.GenerateSource("Name")`

**GenerateSource**()

a function for generating Source back to Python through your Source and Custom Syntax.

* **Name** (Defaults to "Generate" or "Generate.py) argument for setting a name of the output file, If Argument is none supplied Generate.py will be use, example: `GenerateSource(Name="hello")`

--------------

`Py.Cythonize`

**Cythonize**()

Compile your source (Python) using Cython, This helps of you want a performance boost in your app, will generate a file (.pyd,.so) depend on which operating system you are using
> - Warning! This function is useless if you doesnt have a Cython Installled.


-------

`Py.Run` 

**Run**()

Make sure to call this function as the last call, Will run your Parsed source using exec and compile()

------

FlippyError
============

None

------