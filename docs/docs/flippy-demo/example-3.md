Needed Variables
================
These variables are needed by flippy for informations as well as your Syntax's users, if ever you'd like to share it. :)

-----------
* **Syntax**
	* Bool, Flippy use this as Pattern (Syntax If True, Else False) `Syntax = True`
* **Author**
	* String, `Author = 'Your Name'`  your name as a credit.
* **Name**
	* String, `Name = 'Syntax Name'`  your syntax's name.
* **Information** (optional)
	* Long String, `Information = 'The syntax information etc.'` your syntax information.

* **Keywords**
	* Dictionary, e.g `'Key' : 'Value'`, **Key** (string) and can be anything you like, **Value** same As key but this one is the word,syntax,reserved words etc. you would like to replace with your Key Value.

---------------

Create the Syntax
=========

```
Follow the informations above and Save the file into
`/Flippy/Syntax/` with the name you prefer as Python (.py) extension, e.g `LOLCODE.py`

```


--------------
Alternative Method
===============================

If you want to write the syntax file into `/Flippy/Syntax/` through your Source,

Import `Flippy.Flip.SyntaxWrite` method, and call 

**SyntaxWrite**(Source=*MySource*, *"NameOutputFile"*)

Example:

	#!python
	from flippy.Flip import SyntaxWrite
	
	stringSource = """
	
	Syntax = True
	Author = "kram"
	Name = "Crazy Hex Syntax!"
	Information = "It's Crazy!"
	Keywords = {
	
	"77686174616c6f6e676966	" : "if",
	"77686174616c6f6e677072696e74" : "print",
	"77686174616c6f6e673d" : "="
	
	}

	"""
	
	SyntaxWrite(stringSource, "CrazySyntax")

Run and the file `CrazySyntax.py` will be in `flippy/Syntax/` and ready to use! (if you want to.)	

Try your Syntax
================

Example to use your newly created syntax:
	
	#!python
	from flippy.Flip import Py
	source = "77686174616c6f6e677072696e74 'Stupid Syntax'"
	
	haha = Py(source,"CrazySyntax")
	haha.Compile()
	haha.Run()


Output: `Stupid Syntax` which is true.
	
	

---------------------

Example of Syntax Template
===========================
	
--------------
Tagalog.py (Syntax template) Contains the variables mentioned above.

	#!python
	Author = "M. Pequeras"

	Syntax = True

	Name = "Tagalog/Filipino Language (Not literal.)"

	Information = """
	Keywords below are translated into Tagalog language (Not Literal)
	"""

	Keywords = {


	    "at" : "and"
	    , "burahin" : "del"
	    , "iangkat" : "import"
	    , "ipangalan ay" : "as"
	    , "ayaw eh" : "raise"
	    , "isaysay" : "assert"
	    , "i hinto" : "break"
	    , "igrupo sa" : "class"
	    , "tuloy lang" : "continue"
	    , "gawin ang" : "def"
	    , "subukan" : "try"
	    , "kung ayaw" : "except"
	    , "sa wakas" : "finally"
	    , "ay totoo" : "is"
	    , "ibalik" : "return"
	    , "sa mga" : "for"
	    , "sa" : "in"
	    , "patakbuhin" : "exec"
	    , "galing sa" : "from"
	    , "ipakita ang" : "print"
	    , "hindi" : "not"
	    , "habang" : "while"
	    , "hanggat" : "with"
	    , "o kaya" : "or"
	    , "pang lahatan" : "global"
	    , "lambda" : "lambda"
	    , "palagpasin" : "pass"
	    , "hinto at" : "yield"
	    , "kung" : "if"
	    , "kung sakaling" : "elif"
	    , "kung wala" : "else"
	    , "ay" : "="
	    , "parehas" : "=="
	    , "mas malaki sa" : ">"
	    , "mas maliit sa" : "<"
	    , "hindi parehas" : "!="
	    , "malaki at parehas" : ">="
	    , "maliit at parehas" : "=<"
	    , "totoo" : "True"
	    , "wala" : "None"
	    , "hindi totoo" : "False"

	}