As Import files
==================
Seperate files ( **Source.py** & **Syntax.py**)

**Source.py**

```
Strings = """

Cake has Real

until Cake has Fake:
	show me "Cake is now True!"
	Cake has Fake
	
"""
```

***Syntax.py***


	#!python
   	Words = {

		"has" : "=",
		"show me" : "print",
		"until" : "while",
		"Real" : "True",
		"Fake" : "Fake" 
	}
	



**Main.py** (main program)

	#!python
	import Source, Syntax
	from Flippy.Flip import Py

	Flippy = Py(Source.Strings ,Syntax.Words)
	Flippy.Compile()
	Flippy.Run()


Output: `Cake is now True!`



---------------

In-File Example
================
Main.py (Main program)


	#!python
	from flippy.Flip import Py # Import Py()

	MySource = """

	show "Hello Python!"

	"""

	MyCustomWord = {

	"show" : "print"

	}

	# Set the Source and Syntax args.
	Flippy = Py(MySource, MyCustomWord)

	# Compile/Parse Now
	Flippy.Compile()  							

	# Run!
	Flippy.Run()


From File
=========

**source**.txt & **syntax**.txt


	#!python
	from flippy.Flip import Py
	
	OpenSource, OpenSyntax = open('source.txt','r'), open('syntax.txt')
	
	Source,Syntax = OpenSource.read(), OpenSyntax.read()

	# Source, Syntax, Debug
	Flippy = Py(Source,Syntax, True)
	Flippy.Compile()
	Flippy.Run()
	
	OpenSource.close()
	OpenSyntax.close()
	
	



