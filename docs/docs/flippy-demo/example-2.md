How to
============================

Using a Preset Word / Syntax is loading a Pre-set Syntax from `/flippy/Syntax/` directory with {NAME}.py format, Example of the Pre-set word you'll see and also available to use is the `tagalog` syntax (for argument name) and `tagalog.py` as it's file name.

To use the Preset, simple pass the Syntax name as String in Py(Source=Source, Syntax="**tagalog**") for example, and do not forget that every Syntax file has a different words which points to its original word, so mind checking its `Keywords` (dict) variable to understand the translation.

What's inside the Syntax file `tagalog`?

btw, I made the syntax file for the filipino python users so it's basically a filipino language but you can always make yours if you doesn't like the Syntax files in 'Syntax' Directory [Heres how to make your own Syntax File](http://marp.me/flippy/#pages/information.php).

Example of Preset syntax:
	
	#!python
	from flippy.Flip import Py
	
	source = """
	
	ipakita ang "Hello Python"
	

	"""
	
	Flippy = Py(source,"tagalog") # using tagalog preset
	Flippy.Compile()
	Flippy.Run()
	
'ipakita ang' syntax is equivalent to 'show me this' in english, and is set to 'print' keyword.

so the Output would be: `Hello Python`
