## Flippy Installation
---------
 
Installation on Linux and OS X: 
============================
Install with PIP `sudo pip install flippy`
or easy_install `easy_install flippy` 

*or*  via Archive at Sourceforge

```
Get the Archive (Linux):
http://sf.net/projects/flippy 

Extract
$  tar -xvf Flippy-X.X-Linux.tar.gz

Install Now
$ python setup.py install

```

------

Installation on Windows
=======================
Download the Installer (.msi) via SourceForge

```
https://sf.net/projects/flippy
```
Click 'Download' and the .exe installer will be yours shortly.




