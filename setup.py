#from distutils.core import setup
from setuptools import setup

setup(

  name = 'Flippy',
  packages = ['Flippy'], # this must be the same as the name above
  version = '1.1',
  long_description = """
Information:
FlipPy (Flip Python / Filipino Python) is a Python Library for Parsing
defined Python Programming Language syntax into your preferred syntax,
It's like creating a new Clone of Python with your Own preferred Syntax without touching any mighty Python's files.

License:

The MIT License (MIT)

Copyright (c) 2014 Mark Anthony Pequeras

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


""",
  author = 'Mark Anthony Pequeras',
  author_email = 'mark@marp.me',
  url = 'https://marp.me/flippy', # use the URL to the github repo
  download_url = 'https://gitlab.com/coresec/Flippy/commits/1.0', # I'll explain this in a second
  keywords = ['syntax', 'modifier', 'education'], # arbitrary keywords
  classifiers = [],
  license = "MIT License"

)